# Generador de [CUIL](https://en.wikipedia.org/wiki/Cuil)

#### Tecnologias:
- Python 3.11
- tkinter (GUI)

#### Falta:
- [x] Agregar validacion en la entrada
- [ ] Comprobar con datos verdaderos
- [ ] Agregar un checkbox para con/sin guiones
- [x] Campo cuil solo modo solo lectura pero seleccionable
- [ ] Agregar marcos (genero, opciones, ...)
