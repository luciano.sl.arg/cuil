# https://maurobernal.com.ar/cuil/calcular-el-cuil/
# https://es.wikipedia.org/wiki/Clave_%C3%9Anica_de_Identificaci%C3%B3n_Tributaria
# https://es.wikipedia.org/wiki/C%C3%B3digo_de_control

from tkinter import Tk,Entry,Button,mainloop,Frame,Radiobutton,IntVar

class Cuil(Frame):
    def __init__(self,root):
        super(Cuil,self).__init__(master=root)
        self._build()

    def _build(self):
        vcmd=(self.register(self.validateDNI),'%P')
        self.dniInputField=Entry(self,validate='key',validatecommand=vcmd )
        self.dniInputField.pack()
        
        self.cuilOutputField=Entry(self)
        self.cuilOutputField.bind("<Key>", lambda e: "break")
        self.cuilOutputField.pack()

        self.gender=IntVar()
        self.male=Radiobutton(self,text='Varon',variable=self.gender,value=0)
        self.male.select()
        self.male.pack()
        self.female=Radiobutton(self,text='Mujer',variable=self.gender,value=1)
        self.female.pack()

        self.btn=Button(self,text="Generar")
        self.btn.bind('<Button-1>',self.buttonEvent)
        self.btn.pack()

    def validateDNI(self,P):
        return len(P)<=8 and (str.isdigit(P) or P=='')

    def buttonEvent(self,event=None):
        dni=self.dniInputField.get()

        cuil=self.get_cuil(dni,['M','F'][self.gender.get()])
        self.cuilOutputField.delete(0,'end')
        self.cuilOutputField.insert(0,cuil)

    def checksum(self,X,Y,_dni):
        digits=[X,Y]+_dni
        digits.reverse()
        vector=[2,3,4,5,6,7,2,3,4,5]
        Z=0
        for i,val in enumerate(digits):
            Z+=val*vector[i]
            #print(f"i={i} , digits[{i}]={digits[i]} , vector[{j}]={vector[j]} , Z={Z}")
        Z%=11
        Z=11-Z
        return Z

    def get_cuil(self,dni,sex):
        X=2
        _dni=list(dni)
        _dni=map(lambda x:int(x),_dni)
        _dni=list(_dni)
        
        Y={'F':7,'M':0}[sex]
        
        Z=self.checksum(X,Y,_dni)

        if Z==1:
            Y=3
            Z={'M':9,'F':4}[sex]
        
        elif Z >=10:
            Y=3
            Z=self.checksum(X,Y,_dni)

        return f'{X}{Y}-{dni}-{Z}'

if __name__ == '__main__':
    root=Tk()
    root.configure(borderwidth=10)
    root.wm_title('Generar CUIL')
    widget=Cuil(root)
    widget.pack()
    mainloop()
